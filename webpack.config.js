const path = require('path');

module.exports = {
    mode: "development",
    entry: {
        page_main: './src/frontend/index.js',
        page_about: './src/frontend/about.js'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist/js')
    }
};