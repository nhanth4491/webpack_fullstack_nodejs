import _ from 'lodash';
import $ from 'jquery';

$('.upload-btn').on('click', function () {    
    var files = $('#upload-input').get(0).files;
    if (files.length === 0) return;

    var formData = new FormData();

    for (var i = 0; i < files.length; i++) {
        var file = files[i];

        formData.append('uploads[]', file, file.name);
    }

    $.ajax({
        url: '/upload',
        type: 'POST',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            console.log('upload successful!\n' + data);
        }
    });
});

