import $ from "jquery";
import * as userService from "./services/user";

(function(){
    loadUsers();
})();

function loadUsers(){
    userService.getAll().then((msg, err) => {
        $('#div1').text(msg);
    });
}